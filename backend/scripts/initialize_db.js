const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// for development
// require('dotenv').config({ path: '../../.env' });

const User = require('../database/users/model');
const seeds = require('./seeds');

const { NODE_ENV, PROD_SERVER, DEV_SERVER, DB_USER, DB_PASSWORD, DB_NAME } = process.env;
const MONGODB_SERVER = NODE_ENV === 'production' ? PROD_SERVER : DEV_SERVER;

mongoose.connect(
  `mongodb://${DB_USER}:${DB_PASSWORD}@${MONGODB_SERVER}:27017/${DB_NAME}`,
  { authSource: 'admin', useNewUrlParser: true, socketTimeoutMS: 45000 }
);

mongoose.connection.once('open', () => {
  console.log('MongoDB connection established');
});

seeds.users.forEach((item, index) => {
  let user = new User(item);
  user.password = bcrypt.hashSync(user.password, 8);

  user
    .save()
    .then((res) => {
      console.log(`User ${user.username} successfully created`);
      if (seeds.users.length === index + 1) mongoose.connection.close();
    })
    .catch((err) => {
      console.error(`Error creating user: ${err}`);
    });
});
