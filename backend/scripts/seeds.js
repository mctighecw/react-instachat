const users = [
  {
    first_name: 'John',
    last_name: 'Smith',
    username: 'jsmith',
    email: 'john@smith.com',
    password: 'password',
    is_admin: true,
  },
  {
    first_name: 'Susan',
    last_name: 'Williams',
    username: 'swilliams',
    email: 'susan@williams.com',
    password: 'password',
    is_admin: false,
  },
  {
    first_name: 'Sam',
    last_name: 'Jones',
    username: 'sjones',
    email: 'sam@jones.com',
    password: 'password',
    is_admin: false,
  },
];

module.exports = {
  users,
};
