const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const functions = require('../../utils/functions');

const userRoutes = express.Router();
const User = require('./model');

const JWT_SECRET = process.env.JWT_SECRET;

// authentication
userRoutes.route('/register').post((req, res) => {
  const user = new User(req.body);
  const hashedPassword = bcrypt.hashSync(req.body.password, 8);
  user.password = hashedPassword;

  user
    .save()
    .then((user) => {
      res.status(200).send({ status: 'OK', message: 'User registered successfully' });
    })
    .catch((err) => {
      if (err.code === 11000) {
        res.status(403).send({ status: 'Error', message: 'Username or email already taken' });
      } else {
        console.log(err);
        res.status(400).send({ status: 'Error', message: 'Error registering user' });
      }
    });
});

userRoutes.route('/login').post((req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user) {
      res.status(401).send({ status: 'Error', message: 'User not found' });
    } else {
      const valid = functions.checkPassword(user, req.body.password);

      if (valid) {
        // create a token
        const token = jwt.sign({ id: user.id }, JWT_SECRET, {
          expiresIn: 43200, // expires in 12 hours
        });
        const userInfo = functions.getUserInfo(user);
        res.status(200).send({ status: 'OK', token: token, user: userInfo });
      } else {
        res.status(401).send({ status: 'Error', message: 'Invalid login credentials' });
      }
    }
  });
});

userRoutes.route('/check').get((req, res) => {
  const token = req.headers.authorization.split(' ')[1];

  functions.checkJWT(token, JWT_SECRET).then((check) => {
    if (check.valid) {
      let userInfo = {};
      User.findById(check.decoded.id, (err, user) => {
        userInfo = functions.getUserInfo(user);
        res.status(200).send({ status: 'OK', message: 'Token is valid', user: userInfo });
      });
    } else {
      return res.status(401).send({ status: 'Error', message: 'Failed to authenticate token' });
    }
  });
});

// general
userRoutes.route('/get_all').get((req, res) => {
  User.find((err, users) => {
    if (err) {
      console.log(err);
    } else {
      res.json(users);
    }
  });
});

userRoutes.route('/get/:id').get((req, res) => {
  User.findById(req.params.id, (err, user) => {
    res.json(user);
  });
});

userRoutes.route('/update/:id').post((req, res) => {
  User.findById(req.params.id, (err, user) => {
    if (!user) {
      res.status(404).send('User not found');
    } else {
      for (key in req.body) {
        user[key] = req.body[key];
      }
      user
        .save()
        .then((user) => {
          res.json('User updated');
        })
        .catch((err) => {
          res.status(400).send('User could not be updated');
        });
    }
  });
});

userRoutes.route('/delete/:id').delete((req, res) => {
  User.deleteOne({ _id: req.params.id }, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: 'User deleted successfully' });
    }
  });
});

module.exports = userRoutes;
