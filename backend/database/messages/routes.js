const express = require('express');
const messageRoutes = express.Router();
const functions = require('../../utils/functions');
const Message = require('./model');

messageRoutes.route('/get_all').get((req, res) => {
  Message.find((err, messages) => {
    if (err) {
      console.log(err);
    } else {
      res.json(messages);
    }
  });
});

messageRoutes.post('/create', (req, res) => {
  const message = new Message(req.body);
  message
    .save()
    .then((message) => {
      res.status(200).json({ message: 'Message created successfully', new_message: message });
    })
    .catch((err) => {
      res.status(400).send('Error creating message');
    });
});

messageRoutes.route('/edit/:id').post((req, res) => {
  Message.findById(req.params.id, (err, message) => {
    if (!message) {
      res.status(404).send('Message not found');
    } else {
      for (key in req.body) {
        message[key] = req.body[key];
      }
      message
        .save()
        .then((message) => {
          res.json('Message updated');
        })
        .catch((err) => {
          res.status(400).send('Message could not be updated');
        });
    }
  });
});

messageRoutes.route('/delete/:id').delete((req, res) => {
  Message.deleteOne({ _id: req.params.id }, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: 'One message deleted successfully' });
    }
  });
});

messageRoutes.route('/delete_old').delete((req, res) => {
  const yesterday = functions.getYesterdaysDate();
  const formattedDate = functions.formatDate(yesterday);

  Message.deleteMany(
    {
      created_at: {
        // $gte: startDate,
        $lte: yesterday,
      },
    },
    (err) => {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json({ message: `Messages older than ${formattedDate} deleted successfully` });
      }
    }
  );
});

messageRoutes.route('/delete_all').delete((req, res) => {
  Message.deleteMany({}, (err) => {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json({ message: 'All messages deleted successfully' });
    }
  });
});

module.exports = messageRoutes;
