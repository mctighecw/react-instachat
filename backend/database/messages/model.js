const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Message = new Schema({
  username: {
    type: String,
  },
  body: {
    type: String,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Message', Message);
