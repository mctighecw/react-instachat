const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

const formatDate = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('MMMM Do, YYYY');
};

const getYesterdaysDate = () => {
  const today = new Date();
  const currentDateTime = getOffset(today);
  return moment(currentDateTime)
    .subtract(1, 'days')
    .endOf('day')
    .toDate();
};

const getUserInfo = (user) => ({
  first_name: user.first_name,
  last_name: user.last_name,
  username: user.username,
  email: user.email,
  is_admin: user.is_admin,
});

const checkPassword = (user, password) => {
  return bcrypt.compareSync(password, user.password);
};

const checkJWT = (token, secret) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secret, (error, decoded) => {
      if (error) {
        resolve({ valid: false });
      } else {
        resolve({ valid: true, decoded: decoded });
      }
    });
  });
};

const broadcastTotalConnections = (sockets) => {
  const { clientsCount } = sockets.engine;

  // Each client normally has two websockets connections
  const adjustedClients = Math.ceil(clientsCount / 2);

  sockets.emit('broadcast', { type: 'total_connections', data: adjustedClients });
};

const broadcastUpdatedConnections = (sockets) => {
  const { clientsCount } = sockets.engine;

  // Number of connections after user logs out
  const adjustedClients = Math.ceil(clientsCount / 2) - 1;

  sockets.emit('broadcast', { type: 'total_connections', data: adjustedClients });
};

module.exports = {
  formatDate,
  getYesterdaysDate,
  getUserInfo,
  checkPassword,
  checkJWT,
  broadcastTotalConnections,
  broadcastUpdatedConnections,
};
