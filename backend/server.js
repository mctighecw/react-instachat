const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const http = require('http');
const io = require('socket.io');

const functions = require('./utils/functions');
const Message = require('./database/messages/model');

require('dotenv').config({ path: '../.env' });

const PORT = 4000;
const { NODE_ENV, JWT_SECRET, PROD_SERVER, DEV_SERVER, DB_USER, DB_PASSWORD, DB_NAME } = process.env;
const MONGODB_SERVER = NODE_ENV === 'production' ? PROD_SERVER : DEV_SERVER;

const app = express();
app.use(morgan('common'));
app.use(cors());
app.use(bodyParser.json());
app.use(express.static(__dirname + '/build'));

// backend routes
const userRoutes = require('./database/users/routes');
const messageRoutes = require('./database/messages/routes');

app.use('/instachat/users', userRoutes);

// check jwt validity before messages requests
app.use((req, res, next) => {
  const { authorization } = req.headers;
  if (authorization) {
    const token = req.headers.authorization.split(' ')[1];

    functions.checkJWT(token, JWT_SECRET).then((check) => {
      if (check.valid) {
        console.log('Token is valid');
        next();
      } else {
        return res.status(401).send({ status: 'Error', message: 'Invalid token provided' });
      }
    });
  } else {
    return res.status(401).send({ status: 'Error', message: 'No token provided' });
  }
});

app.use('/instachat/messages', messageRoutes);

// database
mongoose.connect(
  `mongodb://${DB_USER}:${DB_PASSWORD}@${MONGODB_SERVER}:27017/${DB_NAME}`,
  { authSource: 'admin', useNewUrlParser: true, socketTimeoutMS: 45000 }
);

mongoose.connection.once('open', () => {
  console.log('MongoDB connection established');
});

// websockets
server = http.Server(app);
sockets = io(server);

sockets.on('connection', (socket) => {
  console.log(`Socket ${socket.id} connected`);
  functions.broadcastTotalConnections(sockets);

  socket.on('broadcast', (socketsData) => {
    console.log(`Backend websockets broadcast received: ${socketsData.type}`);

    if (socketsData.type === 'reload_messages') {
      Message.find((err, messages) => {
        if (err) {
          console.log(err);
        } else {
          const data = JSON.stringify(messages);
          sockets.emit('broadcast', { type: socketsData.type, data });
        }
      });
    } else if (socketsData.type === 'user_disconnect') {
      functions.broadcastUpdatedConnections(sockets);
    } else {
      sockets.emit('broadcast', socketsData);
    }
  });

  socket.on('disconnect', () => {
    console.log(`Socket ${socket.id} disconnected`);
  });
});

// server
server.listen(PORT, () => {
  console.log(`Express web & socket server running on port ${PORT}`);
});
