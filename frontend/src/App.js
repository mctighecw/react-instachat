import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from 'Services/Redux';

import Loading from 'Components/Loading/Loading';
const SignUp = React.lazy(() => import('./pages/SignUp'));
const Login = React.lazy(() => import('./pages/Login'));
const MainRoutes = React.lazy(() => import('Containers/MainRoutes'));

import './styles/global.css';

const App = () => (
  <Provider store={store}>
    <Router>
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route path="/sign-up" render={() => <SignUp />} />
          <Route path="/login" render={() => <Login />} />
          <Route path="/" render={() => <MainRoutes />} />
        </Switch>
      </Suspense>
    </Router>
  </Provider>
);

export default App;
