import axios from 'axios';

export const requestGet = (url, jwt) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'GET',
      headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${jwt}` },
      withCredentials: false,
      responseType: 'json',
      url,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const requestPost = (url, data, jwt) => {
  const headers = jwt
    ? { 'Content-Type': 'application/json', Authorization: `Bearer ${jwt}` }
    : { 'Content-Type': 'application/json' };

  return new Promise((resolve, reject) => {
    axios({
      method: 'POST',
      headers,
      withCredentials: false,
      responseType: 'json',
      url,
      data,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const requestDelete = (url, jwt) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${jwt}` },
      withCredentials: false,
      responseType: 'json',
      url,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
