const getBaseUrl = () => {
  const dev = process.env.NODE_ENV === 'development';

  if (dev) {
    return 'http://localhost:4000';
  } else {
    return '';
  }
};

export const getUrl = (type) => {
  const baseUrl = getBaseUrl();

  switch (type) {
    case 'baseUrl':
      return baseUrl;

    // auth
    case 'register':
      return `${baseUrl}/instachat/users/register`;
    case 'login':
      return `${baseUrl}/instachat/users/login`;
    case 'check':
      return `${baseUrl}/instachat/users/check`;

    // messages
    case 'get_messages':
      return `${baseUrl}/instachat/messages/get_all`;
    case 'create_message':
      return `${baseUrl}/instachat/messages/create`;
    case 'edit_message':
      return `${baseUrl}/instachat/messages/edit`;
    case 'delete_message':
      return `${baseUrl}/instachat/messages/delete`;
    case 'delete_old_messages':
      return `${baseUrl}/instachat/messages/delete_old`;
    case 'delete_all_messages':
      return `${baseUrl}/instachat/messages/delete_all`;

    default:
      return baseUrl;
  }
};
