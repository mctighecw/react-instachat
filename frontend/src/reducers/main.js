import * as types from 'Actions/actionTypes';

const initialState = {
  userInfo: {
    first_name: '',
    last_name: '',
    username: '',
    email: '',
    is_admin: false,
  },
  error: '',
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_USER_INFO:
      return {
        ...state,
        userInfo: { ...action.data },
      };
    case types.CLEAR_USER_INFO:
      return {
        ...state,
        userInfo: initialState.userInfo,
      };
    case types.SET_ERROR_MESSAGE:
      return {
        ...state,
        error: action.data,
      };
    default:
      return state;
  }
};

export default main;
