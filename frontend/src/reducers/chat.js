import * as types from 'Actions/actionTypes';

const initialState = {
  messages: [],
  connectedUsers: 0,
  usersTyping: [],
};

const chat = (state = initialState, action) => {
  switch (action.type) {
    case types.LOAD_ALL_MESSAGES:
      return {
        ...state,
        messages: action.data,
      };
    case types.CLEAR_ALL_MESSAGES:
      return {
        ...state,
        messages: [],
      };
    case types.ADD_NEW_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.data],
      };
    case types.USER_CONNECTIONS:
      return {
        ...state,
        connectedUsers: action.value,
      };
    case types.USER_TYPING:
      const { user, typing } = action.data;
      let usersTyping = state.usersTyping;

      if (typing) {
        if (!usersTyping.includes(user)) {
          usersTyping.push(user);
        }
      } else {
        const index = usersTyping.indexOf(user);
        usersTyping.splice(index, 1);
      }

      return {
        ...state,
        usersTyping,
      };
    default:
      return state;
  }
};

export default chat;
