import { combineReducers } from 'redux';

import main from './main';
import chat from './chat';

const rootReducer = combineReducers({ main, chat });

export default rootReducer;
