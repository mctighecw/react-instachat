import socketIO from 'socket.io-client';
import { getUrl } from 'Network/urls';
import { dispatch } from './Redux';

import {
  userConnections,
  userTyping,
  addNewMessage,
  clearAllMessages,
  loadAllMessages,
} from 'Actions/chatActions';

class Sockets {
  constructor() {
    this.socket = null;
  }

  checkActiveSocket = () => {
    return this.socket !== null;
  };

  connect = () => {
    const isActive = this.checkActiveSocket();
    const baseUrl = getUrl('baseUrl');

    if (!isActive) {
      this.socket = socketIO.connect(
        baseUrl,
        {
          transports: ['websocket'],
          autoConnect: false,
        }
      );

      this.socket.on('connect', () => {
        console.log('Websocket connection established');
      });

      this.socket.on('connect_error', () => {
        this.disconnect();
        console.log('Websocket connection error');
      });

      this.socket.on('reconnect_attempt', () => {
        this.socket.io.opts.transports = ['websocket'];
        console.log('Websocket reconnection attempt');
      });

      this.socket.on('reconnecting', (attemptNumber) => {
        console.log(`Websocket reconnection attempt ${attemptNumber}`);
      });

      this.socket.on('disconnect', () => {
        this.socket.removeAllListeners();
        console.log('Websocket connection disconnected');
      });

      this.socket.open();
    }
  };

  disconnect = () => {
    const isActive = this.checkActiveSocket();

    if (isActive) {
      this.socket.disconnect();
      this.socket = null;
    }
  };

  socketEmit = (type, data) => {
    const isActive = this.checkActiveSocket();

    if (isActive) {
      this.socket.emit('broadcast', { type, data });
    } else {
      console.error('Emit error: websockets not initialized');
    }
  };

  initializeSocketsListener = () => {
    const jwt = localStorage.getItem('jwt');
    const baseUrl = getUrl('baseUrl');
    const socket = socketIO(baseUrl);

    if (jwt) {
      console.log('Initializing sockets listener');

      socket.on('broadcast', (socketsData) => {
        console.log(`Frontend websockets broadcast received: ${socketsData.type}`);

        if (socketsData.type === 'total_connections') {
          console.log(`Total connected clients: ${socketsData.data}`);
          dispatch(userConnections(socketsData.data));
        }

        if (socketsData.type === 'user_typing') {
          dispatch(userTyping(socketsData.data));
        }

        if (socketsData.type === 'new_message') {
          dispatch(addNewMessage(socketsData.data));
        }

        if (socketsData.type === 'reload_messages') {
          const parsed = JSON.parse(socketsData.data);
          dispatch(loadAllMessages(parsed));
        }

        if (socketsData.type === 'clear_all_messages') {
          dispatch(clearAllMessages());
        }
      });
    }
  };
}

export default new Sockets();
