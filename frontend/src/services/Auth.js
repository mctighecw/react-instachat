import Sockets from './Sockets';
import { dispatch } from './Redux';
import { updateUserInfo, clearUserInfo, setErrorMessage } from 'Actions/mainActions';
import { userConnection } from 'Actions/chatActions';
import { getUrl } from 'Network/urls';
import { requestGet, requestPost } from 'Network/requests';

class Auth {
  login = (email, password, history) => {
    const url = getUrl('login');
    const data = JSON.stringify({ email, password });

    requestPost(url, data, null)
      .then((res) => {
        const { token, user } = res.data;
        localStorage.setItem('jwt', token);
        Sockets.connect();
        dispatch(updateUserInfo(user));
        history.push('/chat');
        console.log('Successfully logged in');
      })
      .catch((err) => {
        if (err.response && err.response.status) {
          switch (err.response.status) {
            case 401:
              dispatch(setErrorMessage('Wrong email or password'));
              break;
            case 500:
              dispatch(setErrorMessage('Service not available'));
              break;
            default:
              dispatch(setErrorMessage('An error has occurred'));
          }
        } else {
          dispatch(setErrorMessage('Error connecting to server'));
        }
      });
  };

  checkUserToken = (history) => {
    const jwt = localStorage.getItem('jwt');
    const url = getUrl('check');

    if (jwt) {
      requestGet(url, jwt)
        .then((res) => {
          if (res.status === 200) {
            const { user } = res.data;
            Sockets.connect();
            dispatch(updateUserInfo(user));
            console.log('Checked user token; valid');
          } else {
            this.logout();
            history.push('/login');
          }
        })
        .catch((err) => {
          console.log(err);
          this.logout();
          history.push('/login');
        });
    }
  };

  logout = () => {
    localStorage.removeItem('jwt');
    Sockets.socketEmit('user_disconnect');
    Sockets.disconnect();
    dispatch(clearUserInfo());
    window.location.reload();
    console.log('Successfully logged out');
  };
}

export default new Auth();
