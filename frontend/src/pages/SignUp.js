import React from 'react';
import { withRouter } from 'react-router-dom';
import SignUp from 'Containers/SignUp';

class SignUpPage extends React.Component {
  authenticationCheck = () => {
    const { history, location } = this.props;
    const jwt = localStorage.getItem('jwt');

    if (jwt) {
      history.replace({
        pathname: '/',
        state: { from: location },
      });
    }
  }

  componentDidMount() {
    this.authenticationCheck();
  }

  componentDidUpdate() {
    this.authenticationCheck();
  }
  render() {
    return <SignUp />;
  }
}

export default withRouter(SignUpPage);
