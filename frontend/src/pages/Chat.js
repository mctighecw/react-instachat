import React from 'react';
import TopMenu from 'Containers/TopMenu';
import Chat from 'Containers/Chat';

const ChatPage = () => (
  <div>
    <TopMenu />
    <Chat />
  </div>
);

export default ChatPage;
