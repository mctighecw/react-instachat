import React from 'react';
import TopMenu from 'Containers/TopMenu';
import About from 'Components/About/About';

const AboutPage = () => (
  <div>
    <TopMenu />
    <About />
  </div>
);

export default AboutPage;
