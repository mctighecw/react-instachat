import React from 'react';
import { withRouter } from 'react-router-dom';
import Login from 'Containers/Login';

class LoginPage extends React.Component {
  authenticationCheck = () => {
    const { history, location } = this.props;
    const jwt = localStorage.getItem('jwt');

    if (jwt) {
      history.replace({
        pathname: '/',
        state: { from: location },
      });
    }
  }

  componentDidMount() {
    this.authenticationCheck();
  }

  componentDidUpdate() {
    this.authenticationCheck();
  }
  render() {
    return <Login />;
  }
}

export default withRouter(LoginPage);
