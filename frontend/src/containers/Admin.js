import { connect } from 'react-redux';
import Sockets from 'Services/Sockets';
import { loadAllMessages } from 'Actions/chatActions';
import Admin from 'Components/Admin/Admin';

const mapDispatchToProps = (dispatch) => ({
  handleLoadAllMessages: (data) => dispatch(loadAllMessages(data)),
  handleClearAllMessages: () => Sockets.socketEmit('clear_all_messages'),
});

export default connect(
  null,
  mapDispatchToProps
)(Admin);
