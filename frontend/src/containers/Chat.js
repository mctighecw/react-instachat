import { connect } from 'react-redux';
import { loadAllMessages } from 'Actions/chatActions';
import Chat from 'Components/Chat/Chat';

const mapStateToProps = (state) => ({
  main: state.main,
});

export default connect(
  mapStateToProps,
  null
)(Chat);
