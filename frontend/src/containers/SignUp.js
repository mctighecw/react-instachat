import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setErrorMessage } from 'Actions/mainActions';
import SignUp from 'Components/Auth/SignUp/SignUp';

const mapStateToProps = (state) => ({
  main: state.main,
});

const mapDispatchToProps = (dispatch) => ({
  clearErrorMessage: () => dispatch(setErrorMessage('')),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SignUp)
);
