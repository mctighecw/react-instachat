import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MainRoutes from '../MainRoutes';

const mapStateToProps = (state) => ({
  main: state.main,
});

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(MainRoutes)
);
