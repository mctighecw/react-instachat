import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Auth from 'Services/Auth';
import TopMenu from 'Components/TopMenu/TopMenu';

const mapStateToProps = (state) => ({
  main: state.main,
});

const mapDispatchToProps = (dispatch) => ({
  logoutUser: () => Auth.logout(),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TopMenu)
);
