import { connect } from 'react-redux';
import Sockets from 'Services/Sockets';
import NewMessage from 'Components/NewMessage/NewMessage';

const mapStateToProps = (state) => ({
  main: state.main,
});

const mapDispatchToProps = (dispatch) => ({
  handleEmitNewMessage: (data) => Sockets.socketEmit('new_message', data),
  handleUserTyping: (data) => Sockets.socketEmit('user_typing', data),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewMessage);
