import { connect } from 'react-redux';
import Sockets from 'Services/Sockets';
import { loadAllMessages, addNewMessage } from 'Actions/chatActions';
import MessagesBox from 'Components/MessagesBox/MessagesBox';

const mapStateToProps = (state) => ({
  main: state.main,
  chat: state.chat,
});

const mapDispatchToProps = (dispatch) => ({
  handleLoadAllMessages: (data) => dispatch(loadAllMessages(data)),
  handleReloadMessages: () => Sockets.socketEmit('reload_messages'),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessagesBox);
