import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Auth from 'Services/Auth';
import { setErrorMessage } from 'Actions/mainActions';
import Login from 'Components/Auth/Login/Login';

const mapStateToProps = (state) => ({
  main: state.main,
});

const mapDispatchToProps = (dispatch) => ({
  loginUser: (username, password, history) => Auth.login(username, password, history),
  clearErrorMessage: () => dispatch(setErrorMessage('')),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
