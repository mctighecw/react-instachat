import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Auth from 'Services/Auth';
import Sockets from 'Services/Sockets';

import Chat from 'Pages/Chat';
import About from 'Pages/About';

class MainRoutes extends React.Component {
  authenticationCheck = () => {
    const { main, history, location } = this.props;
    const jwt = localStorage.getItem('jwt');

    if (!jwt) {
      history.replace({
        pathname: '/login',
        state: { from: location },
      });
    }
  }

  tokenCheck = () => {
    const jwt = localStorage.getItem('jwt');
    const { main, location } = this.props;

    if (jwt && location.pathname !== '/' && (!main.userInfo.email || main.userInfo.email.length === 0)) {
      Auth.checkUserToken(history);
    }
  }

  initializeSockets = () => {
    const jwt = localStorage.getItem('jwt');
    const { location } = this.props;

    if (jwt && location.pathname !== '/') {
      Sockets.initializeSocketsListener();
    }
  }

  componentDidMount() {
    this.authenticationCheck();
    this.tokenCheck();
    this.initializeSockets();
  }

  componentDidUpdate() {
    this.authenticationCheck();
  }

  render() {
    return (
      <Switch>
        <Route path="/chat" component={Chat} />
        <Route path="/about" component={About} />
        <Route render={() => <Redirect to="/chat" />} />
      </Switch>
    );
  }
}

export default MainRoutes;
