import moment from 'moment';

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

export const formatDateOnly = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('MMMM Do, YYYY');
};

export const formatTime = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('HH:mm');
};
