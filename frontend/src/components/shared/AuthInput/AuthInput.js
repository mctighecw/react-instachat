import React from 'react';
import './styles.less';

const AuthInput = ({ type, value, name, placeholder, onKeyPressMethod, onChangeMethod }) => (
  <div styleName="container">
    <input
      type={type}
      value={value}
      name={name}
      placeholder={placeholder}
      onKeyPress={onKeyPressMethod}
      onChange={onChangeMethod}
    />
  </div>
);

export default AuthInput;
