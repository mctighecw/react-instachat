import React from 'react';
import './styles.less';

const InputField = ({ value, maxLength, disabled, placeholder, onKeyPress, onChangeMethod }) => (
  <div styleName="container">
    <input
      type="text"
      value={value}
      maxLength={maxLength}
      disabled={disabled || false}
      placeholder={placeholder}
      onKeyPress={onKeyPress}
      onChange={onChangeMethod}
    />
  </div>
);

export default InputField;
