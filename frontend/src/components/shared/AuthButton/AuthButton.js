import React from 'react';
import './styles.less';

const AuthButton = ({ label, onClickMethod }) => (
  <div styleName="container">
    <button styleName="button" onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default AuthButton;
