import React from 'react';
import Footer from 'Components/Footer/Footer';
import { links, logos } from './data';
import ArrowsIcon from 'Assets/arrows-icon.svg';
import './styles.less';

const About = () => {
  const renderLinks = () => {
    const divs = [];

    for (let i = 0; i < links.length; i++) {
      divs.push(
        <span key={i} onClick={() => window.open(`https://${links[i].link}`)} styleName="link">
          {links[i].label}
          {i + 1 === links.length ? ':' : ', '}
        </span>
      );
    }
    return divs;
  }

  const renderLogos = () => {
    const divs = [];

    for (let i = 0; i < logos.length; i++) {
      divs.push(
        <div key={i} styleName="icon-div" style={{ marginLeft: i + 1 === logos.length ? -32 : 0 }}>
          <img src={logos[i].icon} title={logos[i].name} alt={logos[i].name} />
          {i + 1 !== logos.length && <img src={ArrowsIcon} alt="" styleName="arrows-icon" />}
        </div>
      );
    }
    return divs;
  }

  return (
    <div styleName="container">
      <div styleName="content">
        <div styleName="heading">About</div>

        <div styleName="text">
          <span styleName="app-name">React instachat</span> is a real-time, live chat app. Each user can see
          when others are typing. Whenever a message is sent, everyone can see it instantly!
        </div>

        <div styleName="text">This app uses {renderLinks()} a "MERN" tech stack.</div>

        <div styleName="logos">{renderLogos()}</div>
      </div>

      <Footer />
    </div>
  );
};

export default About;
