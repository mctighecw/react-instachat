import ReactLogo from 'Assets/tech/react.svg';
import SocketIOLogo from 'Assets/tech/socketio.svg';
import NodeJSLogo from 'Assets/tech/nodejs.svg';
import MongoDBLogo from 'Assets/tech/mongodb.svg';

export const links = [
  {
    label: 'React',
    link: 'reactjs.org',
  },
  {
    label: 'Redux',
    link: 'redux.js.org',
  },
  {
    label: 'Socket.io',
    link: 'socket.io',
  },
  {
    label: 'Node.js',
    link: 'nodejs.org',
  },
  {
    label: 'Express',
    link: 'expressjs.com',
  },
  {
    label: 'MongoDB',
    link: 'www.mongodb.com',
  },
];

export const logos = [
  {
    name: 'React',
    icon: ReactLogo,
  },
  {
    name: 'Socket.io',
    icon: SocketIOLogo,
  },
  {
    name: 'Node.js',
    icon: NodeJSLogo,
  },
  {
    name: 'MongoDB',
    icon: MongoDBLogo,
  },
];
