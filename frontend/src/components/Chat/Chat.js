import React from 'react';
import './styles.less';

import Admin from 'Containers/Admin';
import MessagesBox from 'Containers/MessagesBox';
import NewMessage from 'Containers/NewMessage';

const Chat = ({ main }) => (
  <div styleName="container">
    {main.userInfo.is_admin && <Admin />}
    <MessagesBox />
    <NewMessage />
  </div>
);

export default Chat;
