import React from 'react';
import LongButton from 'Components/shared/LongButton/LongButton';
import { requestGet, requestDelete } from 'Network/requests';
import { getUrl } from 'Network/urls';
import './styles.less';

const Admin = ({ handleLoadAllMessages, handleClearAllMessages }) => {
  const loadAllMessages = () => {
    const jwt = localStorage.getItem('jwt');
    const url = getUrl('get_messages');

    if (jwt) {
      requestGet(url, jwt)
        .then((res) => {
          handleLoadAllMessages(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  const handleDeleteOld = () => {
    const confirmDeletion = confirm('Are you sure that you want to delete all messages before today?');

    if (confirmDeletion) {
      const jwt = localStorage.getItem('jwt');
      const url = getUrl('delete_old_messages');

      requestDelete(url, jwt)
        .then((res) => {
          loadAllMessages();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  const handleDeleteAll = () => {
    const confirmDeletion = confirm('Are you sure that you want to delete all messages?');

    if (confirmDeletion) {
      const jwt = localStorage.getItem('jwt');
      const url = getUrl('delete_all_messages');

      requestDelete(url, jwt)
        .then((res) => {
          handleClearAllMessages();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  return (
    <div styleName="container">
      <LongButton label="Delete old messages" onClickMethod={() => handleDeleteOld()} />
      <div styleName="spacer" />
      <LongButton label="Delete all messages" onClickMethod={() => handleDeleteAll()} />
    </div>
  );
};

export default Admin;
