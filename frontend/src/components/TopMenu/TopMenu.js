import React from 'react';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import './styles.less';

class TopMenu extends React.Component {
  handleLogout = () => {
    const { main, history } = this.props;
    this.props.logoutUser(main.userInfo);
    history.push('/');
  }

  render() {
    const { history, match } = this.props;
    const jwt = localStorage.getItem('jwt');

    const links = [
      {
        label: 'Chat',
        path: '/chat',
      },
      {
        label: 'About',
        path: '/about',
      },
    ];

    return (
      <div styleName="container">
        <div styleName="title">React instachat</div>

        <div styleName="right">
          {links.map((item, index) => {
            const style = match.path === item.path ? `link active` : `link normal`;

            return (
              <div key={index} styleName={style} onClick={() => history.push(item.path)}>
                {item.label}
              </div>
            );
          })}

          {jwt && (
            <div styleName="logout-button">
              <SmallButton label="Logout" onClickMethod={this.handleLogout} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default TopMenu;
