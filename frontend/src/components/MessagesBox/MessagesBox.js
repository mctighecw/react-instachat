import React from 'react';
import { requestGet, requestPost, requestDelete } from 'Network/requests';
import { getUrl } from 'Network/urls';
import { formatDateOnly, formatTime } from 'Utils/functions';
import DeleteIcon from 'Assets/delete-icon.svg';
import EraseIcon from 'Assets/erase-icon.svg';
import './styles.less';

class MessagesBox extends React.Component {
  constructor(props) {
    super(props);
    this.scrollBox = null;
    this.messageBox = React.createRef();
  }

  scrollToBottom = () => {
    this.scrollBox = setTimeout(() => {
      const messageBox = this.messageBox.current;
      if (messageBox) messageBox.scrollTop = messageBox.scrollHeight;
    }, 200);
  }

  loadAllMessages = () => {
    const jwt = localStorage.getItem('jwt');
    const url = getUrl('get_messages');

    if (jwt) {
      requestGet(url, jwt)
        .then((res) => {
          this.props.handleLoadAllMessages(res.data);
          this.scrollToBottom();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  handleDeleteMessage = (id) => {
    const jwt = localStorage.getItem('jwt');
    const url = `${getUrl('delete_message')}/${id}`;

    if (jwt) {
      requestDelete(url, jwt)
        .then((res) => {
          this.props.handleReloadMessages();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  handleRedactMessage = (id) => {
    const jwt = localStorage.getItem('jwt');
    const url = `${getUrl('edit_message')}/${id}`;
    const redacted = '-- message removed --';
    const data = JSON.stringify({ body: redacted });

    if (jwt) {
      requestPost(url, data, jwt)
        .then((res) => {
          this.props.handleReloadMessages();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  componentDidMount() {
    this.loadAllMessages();
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.chat.messages !== this.props.chat.messages) {
      this.scrollToBottom();
    }
  }

  componentWillUnmount() {
    clearTimeout(this.scrollBox);
  }

  render() {
    const { main, chat } = this.props;
    const usersLabel = chat.connectedUsers === 1 ? 'user' : 'users';
    const usersOnline = `${chat.connectedUsers} ${usersLabel} currently online`;

    return (
      <div styleName="container">
        <div styleName="heading">Live instafeed</div>
        <div styleName="users-online">{usersOnline}</div>
        <div ref={this.messageBox} styleName="messages-box">
          {chat && chat.messages && chat.messages.length > 0 ? (
            chat.messages.map((item, index) => (
              /* show initial date and date changes */
              <div key={`outer_${index}`}>
                <div
                  styleName="chat-date"
                  style={{
                    display:
                      index === 0 ||
                      (chat.messages.length > 1 &&
                        index !== 0 &&
                        formatDateOnly(chat.messages[index - 1].created_at) !==
                          formatDateOnly(item.created_at))
                        ? 'inline-block'
                        : 'none',
                  }}
                >
                  {formatDateOnly(item.created_at)}
                </div>

                <div key={`inner_${index}`} styleName="message">
                  <div styleName="user">{item.username}</div>
                  <div styleName="datetime">{formatTime(item.created_at)}</div>
                  {main.userInfo.is_admin && (
                    <div styleName="admin-icons">
                      <img src={DeleteIcon} alt="Delete" onClick={() => this.handleDeleteMessage(item._id)} />
                      <img src={EraseIcon} alt="Redact" onClick={() => this.handleRedactMessage(item._id)} />
                    </div>
                  )}
                  <div styleName="body">{item.body}</div>
                </div>
              </div>
            ))
          ) : (
            <div styleName="no-messages">no messages yet</div>
          )}
          {chat &&
            chat.usersTyping &&
            chat.usersTyping.length > 0 &&
            chat.usersTyping.map((item, index) => {
              return (
                <div key={`typing_${index}`} styleName="user-typing">
                  <span>{item}</span>
                  {` is typing...`}
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default MessagesBox;
