import React from 'react';
import './styles.less';

const Footer = () => (
  <div styleName="container">
    <div styleName="copyright">
      Coded by Hand.
      <br />
      &copy; 2019 Christian McTighe.
    </div>
  </div>
);

export default Footer;
