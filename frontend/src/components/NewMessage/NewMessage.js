import React from 'react';
import InputField from 'Components/shared/InputField/InputField';
import SmallButton from 'Components/shared/SmallButton/SmallButton';
import { requestPost } from 'Network/requests';
import { getUrl } from 'Network/urls';
import './styles.less';

class NewMessage extends React.Component {
  state = {
    text: '',
  }

  handleChangeInput = (event) => {
    const { main, handleUserTyping } = this.props;
    const { text } = this.state;
    const { value } = event.target;
    const { username } = main.userInfo;

    this.setState({ text: value });

    if (value.length === 1) {
      handleUserTyping({ user: username, typing: true });
    }
    if (value.length === 0) {
      handleUserTyping({ user: username, typing: false });
    }
  }

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleSendMessage();
    }
  }

  handleSendMessage = () => {
    const { main, handleUserTyping, handleEmitNewMessage } = this.props;
    const jwt = localStorage.getItem('jwt');
    const { text } = this.state;

    if (jwt && text !== '') {
      const url = getUrl('create_message');
      const data = JSON.stringify({ username: main.userInfo.username, body: text });

      handleUserTyping({ user: main.userInfo.username, typing: false });

      requestPost(url, data, jwt)
        .then((res) => {
          handleEmitNewMessage(res.data.new_message);
          this.setState({ text: '' });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  render() {
    const { text } = this.state;

    return (
      <div styleName="container">
        <div styleName="input-field">
          <InputField
            value={text}
            maxLength="100"
            placeholder="new message..."
            onKeyPress={this.handleInputKeyPress}
            onChangeMethod={this.handleChangeInput}
          />
        </div>
        <SmallButton label="send" onClickMethod={this.handleSendMessage} />
      </div>
    );
  }
}

export default NewMessage;
