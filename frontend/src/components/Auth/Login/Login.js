import React from 'react';
import AuthInput from 'Components/shared/AuthInput/AuthInput';
import AuthButton from 'Components/shared/AuthButton/AuthButton';

import ChatIcon from 'Assets/chat-icon.svg';
import '../styles.less';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'john@smith.com',
      password: 'password',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  }

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleLoginUser();
    }
  }

  handleLoginUser = () => {
    const { email, password } = this.state;
    const { history } = this.props;

    this.setState({ error: '' });

    if (email !== '' && password !== '') {
      this.props.loginUser(email, password, history);
      this.props.clearErrorMessage();
    } else {
      this.setState({ error: 'Please fill out both fields' });
    }
  }

  handleAuthLink = () => {
    this.clearMessages();
    this.props.history.push('/sign-up');
  }

  clearMessages = () => {
    this.setState({ error: '' });
    this.props.clearErrorMessage();
  }

  render() {
    const { email, password } = this.state;
    const error = this.state.error || this.props.main.error;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="left-box">
            <div styleName="content">
              <div styleName="title">React instachat</div>
              <img src={ChatIcon} alt="instachat" />
            </div>
          </div>
          <div styleName="right-box">
            <div styleName="content">
              <div styleName="heading">Login</div>

              <AuthInput
                type="text"
                value={email}
                name="email"
                placeholder="Email"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                onKeyPressMethod={this.handleInputKeyPress}
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="error">{error}</div>

              <AuthButton label="Log in" onClickMethod={this.handleLoginUser} />

              <div styleName="link" onClick={this.handleAuthLink}>
                Go to sign up
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
