import React from 'react';
import { getUrl } from 'Network/urls';
import { requestPost } from 'Network/requests';
import AuthInput from 'Components/shared/AuthInput/AuthInput';
import AuthButton from 'Components/shared/AuthButton/AuthButton';

import ChatIcon from 'Assets/chat-icon.svg';
import '../styles.less';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      username: '',
      email: '',
      password: '',
      passwordReview: '',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  }

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleSignUpUser();
    }
  }

  handleSignUpUser = () => {
    const { first_name, last_name, username, email, password, passwordReview } = this.state;
    const { history } = this.props;

    this.clearMessages();

    if (first_name !== '' && last_name !== '' && username !== '' && email !== '' && password !== '') {
      if (password !== passwordReview) {
        this.setState({ error: 'Your passwords do not match' });
      } else {
        const url = getUrl('register');
        const data = JSON.stringify({ first_name, last_name, username, email, password });

        requestPost(url, data, null)
          .then((res) => {
            console.log(res);
            history.push('/login');
            console.log('Successfully signed up');
          })
          .catch((err) => {
            console.log(err);

            if (err.response.status === 403) {
              this.setState({ error: 'Username or email already taken' });
            } else {
              this.setState({ error: 'An error has occurred' });
            }
          });
      }
    } else {
      this.setState({ error: 'Please fill out all fields' });
    }
  }

  handleAuthLink = () => {
    this.clearMessages();
    this.props.history.push('/login');
  }

  clearMessages = () => {
    this.setState({ error: '' });
    this.props.clearErrorMessage();
  }

  render() {
    const { first_name, last_name, username, email, password, passwordReview } = this.state;
    const error = this.state.error || this.props.main.error;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="left-box">
            <div styleName="content">
              <div styleName="title">React instachat</div>
              <img src={ChatIcon} alt="instachat" />
            </div>
          </div>
          <div styleName="right-box">
            <div styleName="content">
              <div styleName="heading">Sign Up</div>

              <AuthInput
                type="text"
                value={first_name}
                name="first_name"
                placeholder="First name"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="text"
                value={last_name}
                name="last_name"
                placeholder="Last name"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="text"
                value={username}
                name="username"
                placeholder="Username"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="text"
                value={email}
                name="email"
                placeholder="Email"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={passwordReview}
                name="passwordReview"
                placeholder="Password (review)"
                onKeyPress={this.handleInputKeyPress}
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="error">{error}</div>

              <AuthButton label="Sign up" onClickMethod={this.handleSignUpUser} />

              <div styleName="link" onClick={this.handleAuthLink}>
                Go to login
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SignUp;
