import React from 'react';
import './styles.less';

const Loading = () => (
  <div styleName="container">
    <p>Loading...</p>
  </div>
);

export default Loading;
