import * as types from './actionTypes';

export const updateUserInfo = (data) => ({
  type: types.UPDATE_USER_INFO,
  data,
});

export const clearUserInfo = () => ({
  type: types.CLEAR_USER_INFO,
});

export const setErrorMessage = (data) => ({
  type: types.SET_ERROR_MESSAGE,
  data,
});
