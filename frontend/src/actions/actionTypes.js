// Main
export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';
export const CLEAR_USER_INFO = 'CLEAR_USER_INFO';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';

// Chat
export const LOAD_ALL_MESSAGES = 'LOAD_ALL_MESSAGES';
export const CLEAR_ALL_MESSAGES = 'CLEAR_ALL_MESSAGES';
export const ADD_NEW_MESSAGE = 'ADD_NEW_MESSAGE';
export const USER_CONNECTIONS = 'USER_CONNECTIONS';
export const USER_TYPING = 'USER_TYPING';
