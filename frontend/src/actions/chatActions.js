import * as types from './actionTypes';

export const loadAllMessages = (data) => ({
  type: types.LOAD_ALL_MESSAGES,
  data,
});

export const clearAllMessages = () => ({
  type: types.CLEAR_ALL_MESSAGES,
});

export const addNewMessage = (data) => ({
  type: types.ADD_NEW_MESSAGE,
  data,
});

export const userConnections = (value) => ({
  type: types.USER_CONNECTIONS,
  value,
});

export const userTyping = (data) => ({
  type: types.USER_TYPING,
  data,
});
