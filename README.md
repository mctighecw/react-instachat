# README

A real-time chat app using React, Node.js, and websockets.

Users can sign up, log in, and chat with one another. New messages appear to all users instantly.

## App Information

App Name: react-instachat

Created: May-June 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-instachat)

Production: [Link](https://instachat.mctighecw.site)

## Tech Stack

_MERN_ (MongoDB, Express, React, Node)

_Frontend_

- React
- Redux
- React Router
- Socket.io Client
- Less & CSS Modules
- Prettier

_Backend_

- Node.js
- Express
- JWT Authentication
- Socket.io
- MongoDB
- Mongoose

## Run on Docker

1. Add .env and .env_ssl files to project root

2. Run commands

```
$ docker-compose up -d --build
$ source init_docker_db.sh
```

Project running at `localhost:80`

## Credits

Icons courtesy of Freepik at [Flaticon](https://www.flaticon.com/free-icons/freepik)

Last updated: 2025-02-23
